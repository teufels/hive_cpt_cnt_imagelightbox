plugin.tx_hivecptcntimagelightbox_hivelightboxgallery {
	view {
		# cat=plugin.tx_hivecptcntimagelightbox_hivelightboxgallery/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:hive_cpt_cnt_imagelightbox/Resources/Private/Templates/
		# cat=plugin.tx_hivecptcntimagelightbox_hivelightboxgallery/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:hive_cpt_cnt_imagelightbox/Resources/Private/Partials/
		# cat=plugin.tx_hivecptcntimagelightbox_hivelightboxgallery/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:hive_cpt_cnt_imagelightbox/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_hivecptcntimagelightbox_hivelightboxgallery//a; type=string; label=Default storage PID
		storagePid =
	}
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

########################################
## INCLUDES FOR STAGING && PRODUCTION ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cpt_cnt_imagelightbox/Configuration/TypoScript/Constants/Production" extensions="txt">

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[globalString = ENV:HTTP_HOST=development.*]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cpt_cnt_imagelightbox/Configuration/TypoScript/Constants/Development" extensions="txt">
[global]